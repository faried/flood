;;;; board.lisp
;;;; Solve a board similar to https://play.google.com/store/apps/details?id=com.labpixies.flood

(defpackage :flood
  (:use :common-lisp))

(in-package :flood)

;; copy-array
(ql:quickload "alexandria")

(defparameter *colors* (list :d :b :g :y :r :p)
  "Dark-Blue Blue Green Yellow Red Pink")

(defun random-color (&key (not-color nil) (previous nil) (colors *colors*))
  "Return a random color.
If `not-color' is specified, it is removed from the list before selection.
A `previous' color can be provided to bias the selection in favor of that color."
  (let* ((colors-1 (remove not-color colors))
         (colors-2 (if (null previous) colors-1
                       (cons previous colors-1))))
    (if colors-2
        (elt colors-2 (random (length colors-2)))
        (elt *colors* (random (length *colors*))))))

(defun make-board (&optional (rows 12) (columns 12) (color nil))
  "Make an empty board."
  (make-array (list rows columns) :initial-element color))

(defun reset-board (board &optional color)
  "Reset the board to empty.  If a color is provided, the board tiles are set to that color."
  (loop
     for i from 0 below (array-total-size board)
     do (setf (row-major-aref board i) color)
     finally (return board)))

(defun setup-board (board)
  "Generate a randomized board."
  (loop
     for i from 0 below (array-total-size board)
     with color = nil
     do (progn
          (setf color (random-color :previous color))
          (setf (row-major-aref board i) color))
     finally (return board)))

(defun setup-board-game (board)
  "Generate a board based on an actual game."
  (let ((tiles (list :g :d :g :g :b :g :b :g :d :b :b :d
                     :g :g :d :b :d :g :d :g :d :d :b :g
                     :g :d :d :g :d :g :d :d :d :g :d :b
                     :d :b :d :b :d :g :b :d :b :g :d :b
                     :b :d :g :b :d :d :d :d :b :g :d :g
                     :g :b :g :d :b :d :b :b :g :g :d :d
                     :b :b :b :d :b :b :d :g :g :d :b :d
                     :d :d :d :g :d :b :g :d :g :g :d :b
                     :g :b :d :b :d :b :d :g :d :b :g :g
                     :d :d :g :g :d :b :d :g :g :b :g :g
                     :d :b :g :d :d :g :g :d :b :b :g :g
                     :d :g :g :d :d :b :d :b :d :b :b :d)))
    (loop for i from 0 below (array-total-size board)
       do (setf (row-major-aref board i) (nth i tiles))
       finally (return board))))

(defun print-board (board &optional trailing-newline)
  "From https://stackoverflow.com/a/18932090 with an annoying hack to not
display a space after the last column."
  (fresh-line)
  (let ((rows (array-dimension board 0))
        (columns (array-dimension board 1)))
    (dotimes (r rows)
      (dotimes (c (- columns 1))
        (format t "~a " (aref board r c)))
      (format t "~a" (aref board r (- columns 1)))
      (terpri)))
  (when trailing-newline
    (terpri)))

(defun solved-board-p (board)
  "The board is solved when all the elements are of the same color."
  (loop
     with first = (aref board 0 0)
     for i from 0 below (array-total-size board)
     always (eq first (row-major-aref board i))))

(defun possible-tiles (board row col &key (same-color t))
  "Return the (row cell) pairs adjacent to the given tile.
Returns the tiles with the same color as the given tile
unless `same-color' is nil."
  (destructuring-bind (rows columns) (array-dimensions board)
    (let ((initial (aref board row col))
          (tiles (list (list (- row 1) col)
                       (list (+ row 1) col)
                       (list row (- col 1))
                       (list row (+ col 1)))))
      (remove-if #'(lambda (rc)
                     (or (< (car rc) 0)
                         (< (cadr rc) 0)
                         (>= (car rc) rows)
                         (>= (cadr rc) columns)
                         (if same-color
                             (not (eq initial (aref board (car rc) (cadr rc))))
                             (eq initial (aref board (car rc) (cadr rc))))))
                 tiles))))

(defun connected-tiles (board &optional (start-row 0) (start-col 0))
  "Return a list of tiles with the same color as (START-ROW START-COL)."
  (loop
     with visited = (list (list start-row start-col))
     with to-visit = (possible-tiles board start-row start-col)
     with tile = nil
     while to-visit
     do (progn
          (setf tile (pop to-visit))
          (push tile visited)
          (let ((maybe (remove-if #'(lambda (tile) (or (member tile visited :test #'equal)
                                                       (member tile to-visit :test #'equal)))
                                  (possible-tiles board (car tile) (cadr tile)))))
            (if (or maybe to-visit)
                (setf to-visit (append to-visit maybe)))))
     finally (return (reverse visited))))

(defun flood-board (board color)
  "Try to fill the board with a color.
Starting at (0 0), find all the connected tiles with the same color.
Change their color to the given one."
  (loop
     for (r c) in (connected-tiles board)
     do (setf (aref board r c) color)
     finally (return board)))

;;; Solvers

(defun solve-random (board &optional best)
  "Repeatedly pick a random color and try to fill the board with it.
If `tries' exceeds `best', we exit with (TRIES NIL)."
  (loop
     with board = (alexandria:copy-array board)
     with tries = 0
     with color = nil
     with colors = nil
     until (solved-board-p board)
     do (progn
          (setf color (random-color :not-color (aref board 0 0)))
          (push color colors)
          (setf board (flood-board board color))
          (incf tries))
     if (and best (> tries best))
     return (list tries nil)
     finally (return (list tries (reverse colors)))))

(defun possible-colors (board)
  "Return a list of colors adjacent to the tiles connected to (0 0)."
  (loop
     with visited = nil
     with colors = nil
     with connected = (connected-tiles board)
     for (r c) in connected
     do (loop
           with possibles = (possible-tiles board r c :same-color nil)
           for tile in possibles
           do (progn
                (if (and (not (member tile connected :test #'equal))
                         (not (member tile visited :test #'equal)))
                    (progn
                      (push tile visited)
                      (push (aref board (car tile) (cadr tile)) colors)))))
     finally (return (list colors visited))))

(defun solve-with-possible-colors (board &optional best)
  "Pick a random color from a list of possible colors.
If `tries' exceeds `best', we exit with (TRIES NIL)."
  (loop
     with board = (alexandria:copy-array board)
     with tries = 0
     with color = nil
     with colors = nil
     until (solved-board-p board)
     with initial = (aref board 0 0)
     do (progn
          (setf color (random-color :not-color initial :colors (car (possible-colors board))))
          (push color colors)
          (setf board (flood-board board color))
          (incf tries))
     if (and best (> tries best))
     return (list tries nil)
     finally (return (list tries (reverse colors)))))

(defun adjacent-blocks (board tiles)
  "Return a list of colors and blocks adjacent to the main
work area, sorted by the size of the blocks in descending order."
  (let ((color-tiles (make-hash-table)) ;; key: color, value: list of tiles
        (blocklist nil)) ;; pairs of (number of tiles, color)
    (loop
       with color = nil
       with current-tiles = nil
       for (r c) in tiles
       do (progn
            (setf color (aref board r c))
            (setf current-tiles (gethash color color-tiles))
            (loop
               for tile in (connected-tiles board r c)
               do (pushnew tile current-tiles))
            (setf (gethash color color-tiles) current-tiles)))
    ;; (loop for value being the hash-values of color-tiles
    ;;    using (hash-key key)
    ;;    do (format t "~&~A -> ~A" key value))
    (maphash #'(lambda (k v) (push (list (length v) k) blocklist)) color-tiles)
    (mapcar #'cadr (sort blocklist #'> :key #'car))))

(defun solve-with-largest-blocks (board &optional best)
  "Pick the color used by the largest adjacent block.
If `tries' exceeds `best', we exit with (TRIES NIL)."
  (loop
     with board = (alexandria:copy-array board)
     with tries = 0
     with color = nil
     with colors = nil
     until (solved-board-p board)
     do (progn
          ;; (print (adjacent-blocks (cadr (possible-colors))))
          ;; (format t "~&tries ~D" tries)
          (setf color (car (adjacent-blocks board (cadr (possible-colors board)))))
          (push color colors)
          (setf board (flood-board board color))
          (incf tries))
     if (and best (> tries best))
     return (list tries nil)
     finally (return (list tries (reverse colors)))))

(defun solve-r (board &optional (best 0) (tries 0) colors)
  (cond ((and (> best 0) (> tries best)) nil)
        ((solved-board-p board)
         (format t "~&solved: ~D ~S" tries (reverse colors))
         (list tries (reverse colors)))
        (t
         (car
          (sort
           (remove-if #'(lambda (res) (null res))
                      (mapcar #'(lambda (color)
                                  (solve-r (flood-board (alexandria:copy-array board) color)
                                           best (1+ tries) (cons color colors)))
                              (adjacent-blocks board (cadr (possible-colors board)))))
           #'< :key #'car)))))

(defun test-solve-r (&optional board)
  (if (null board)
      (setf board (setup-board-game (make-board))))
  (let ((quick (solve-with-largest-blocks board)))
    (format t "~&can be solved with: ~S" quick)
    (solve-r board (1- (car quick)))))

(defun test-solve-r-2 (&optional board)
  (if (null board)
      (setf board (setup-board-game (make-board))))
  (let ((quick (solve-with-largest-blocks board)))
    (format t "~&can be solved with: ~S" quick)
    (loop
       for depth from (ceiling (/ (car quick) 2)) below (car quick)
       with res = nil
       until res
       do (progn
            (format t "~&depth ~D" depth)
            (setf res (solve-r board depth)))
       finally (return res))))

(defun find-all-colors (board)
  "Find all colors in a board."
  (loop
     with colors = nil
     for i from 0 below (array-total-size board)
     do (pushnew (row-major-aref board i) colors)
     finally (return (reverse colors))))

(defun solve-with-color-cycling (board &optional best)
  (loop
     with board = (alexandria:copy-array board)
     with colors-to-try = (find-all-colors board)
     with colors = nil
     with color = nil
     with tries = 0
     until (solved-board-p board)
     do (progn
          (setf color (car colors-to-try))
          (push color colors)
          (setf colors-to-try (append (cdr colors-to-try) (list color)))
          (setf board (flood-board board color))
          (incf tries))
     if (and best (> tries best))
     return (list tries nil)
     finally (return (list tries (reverse colors)))))


(defun test (&key
               (board (setup-board-game (make-board)))
               (max-turns 64) (iterations 500) (solver #'solve-with-possible-colors))
  (let* ((work-board nil)
         (attempts (loop
                      with maxval = max-turns
                      with attempt = nil
                      for i from 1 to iterations
                      do (progn
                           (setf work-board (alexandria:copy-array board))
                           (setf attempt (funcall solver work-board maxval)))
                      if (not (null (cadr attempt)))
                      collect attempt
                      if (< (car attempt) maxval)
                      do (progn (setf maxval (car attempt)) (format t "~&maxval set to ~D" maxval)))))
    (car (sort attempts #'< :key #'car))))

;; eof
